const nombre = document.getElementById("nombre")
const apellido = document.getElementById("apellido")
const telefono= document.getElementById("telefono")
const altura = document.getElementById("altura")
const peso = document.getElementById("peso")
const hijos = document.getElementById("hijos")
const emaill = document.getElementById("emaill")
const form = document.getElementById("form")

form.addEventListener("submit", e=>{
    e.preventDefault()
    if(telefono.value.length <10 ){
        alert("El número de teléfono debe tener 10 digitos")
    }
    if(nombre.value.length <=2){
        alert("El nombre debe tener más de 2 letras")
    }
    if(apellido.value.length <=2){
        alert("El apellido debe tener más de 2 letras")
    }
    if(altura.value <=30){
        alert("la altura debe ser mayor a 30 cm")
    }
    if(peso.value <=2){
        alert("El peso debe ser mayor a 2 kg")
    }
})